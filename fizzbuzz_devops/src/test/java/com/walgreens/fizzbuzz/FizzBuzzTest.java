package com.walgreens.fizzbuzz;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;



public class FizzBuzzTest {
	
	
    @Test 
    public void fizzbuzz_success() { 
        // set up
    	FizzBuzz spy = PowerMockito.spy(new FizzBuzz());
    	
    	// run test
    	spy.run();
    	
    	// verify results
    	verify(spy, Mockito.times(27)).printFizz(anyObject());
    	verify(spy, Mockito.times(14)).printBuzz(anyObject());
    	verify(spy, Mockito.times(6)).printBoth(anyObject());
    	verify(spy, Mockito.times(53)).printNumber(anyObject(), anyInt());
        
    }
    
}
