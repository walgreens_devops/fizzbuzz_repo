package com.walgreens.fizzbuzz;

public class FizzBuzz {

	/*
	 * Write a program that prints the numbers from 1 to 100. But for multiples
	 * of three print �Fizz� instead of the number and for the multiples of five
	 * print �Buzz�. For numbers which are multiples of both three and five
	 * print �FizzBuzz�.
	 */

	public String run() {
		
		StringBuffer sb = new StringBuffer();

		for (int i = 1; i < 101; i++) {
			if (i % 3 == 0 && i % 5 == 0) {
				printBoth(sb);
			} else if (i % 3 == 0) {
				printFizz(sb);
			} else if (i % 5 == 0) {
				printBuzz(sb);
			} else {
				printNumber(sb, i);
			}
		}
		
		return sb.toString();

	}

	protected void printFizz(StringBuffer sb) {
		sb.append("Fizz" + "<br>");
	}

	protected void printBuzz(StringBuffer sb) {
		sb.append("Buzz" + "<br>");
	}

	protected void printBoth(StringBuffer sb) {
		sb.append("FizzBuzz" + "<br>");
	}

	protected void printNumber(StringBuffer sb, int i) {
		sb.append(i + "<br>");
	}
}
