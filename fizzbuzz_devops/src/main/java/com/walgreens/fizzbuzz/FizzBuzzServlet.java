package com.walgreens.fizzbuzz;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*; 

public class FizzBuzzServlet extends HttpServlet { 

	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");

		// Actual logic goes here.
		String message = new FizzBuzz().run();
		PrintWriter out = response.getWriter();
		out.println("<h1>" + message + "</h1>");
	}

	public void destroy() {
		// do nothing.
	}
}
